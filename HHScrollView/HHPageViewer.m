//
//  HHPageViewer.m
//  HHScrollView
//
//  Created by Hichem iMac on 15/05/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "HHPageViewer.h"

typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;


@interface HHPageViewer ()<UIScrollViewDelegate>

@property (nonatomic, retain) UIScrollView *hHPageViewerScrollView;
@property (nonatomic, retain) UIView *selectedPageIndicatorView ;
@property (nonatomic, assign) CGFloat lastContentOffset;






@end

NSString *const hHPageViewerPageTitleColorForDisplayedStateKey       = @"hHPageViewerPageTitleColorForDisplayedStateKey";
NSString *const hHPageViewerPageTitleColorForNotDisplayedStateKey    = @"hHPageViewerPageTitleColorForNotDisplayedStateKey";

NSString *const hHPageViewerDisplayedPageIndicatorViewColorKey       = @"hHPageViewerDisplayedPageIndicatorViewColorKey";

NSString *const hHPageViewerScrollToPageAnimatedKey                  = @"hHPageViewerScrollToPageAnimatedKey";


#pragma Default Options

static UIColor *hHPageTitleColorForDisplayedStateDefault = nil;
static UIColor *hHPageTitleColorForNotDisplayedStateDefault = nil;

static UIColor *hHDisplayedPageIndicatorViewColorDefault = nil;

static NSNumber *hHScrollToPageAnimatedDefault = nil;





@implementation HHPageViewer{
    
    CGRect _hHPageViewerScrollViewFrame;
    float  _titleButtonWidth;
    
    NSMutableArray *_titlesBtnArray;
    
    BOOL _pageTitlePressed;
    
    NSMutableDictionary *_options;
    NSDictionary *_hHPageViewerKeyClassMap;



}

@synthesize numberOfPages = _numberOfPages;
@synthesize datasource = _datasource;
@synthesize hHPageViewerOptions = _hHPageViewerOptions ;

- (id)initWithFrame:(CGRect)frame datasource:(id <HHPageViewerDatasource>)datasource andOptions:(NSDictionary *)options
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        _datasource = datasource;
        _hHPageViewerOptions = (options)?[options mutableCopy]:[NSMutableDictionary dictionary];
        _titlesBtnArray = [NSMutableArray array];
        
        [self initialiseDefaultOptions];
        [self initialiseOptions];
        
        float scrollViewHeight = frame.size.height -(frame.size.height / 10);
        float scrollViewY =  frame.size.height / 10;
        
        NSLog(@"scrollViewY == %f",(frame.size.height / 10));
        
        _hHPageViewerScrollViewFrame = CGRectMake(frame.origin.x, scrollViewY, self.frame.size.width, scrollViewHeight);
    
        
        self.hHPageViewerScrollView  = [[UIScrollView alloc] initWithFrame:_hHPageViewerScrollViewFrame];
        self.hHPageViewerScrollView.delegate = self;
        
        self.hHPageViewerScrollView.showsHorizontalScrollIndicator = NO;
        self.hHPageViewerScrollView.pagingEnabled = YES;
        
        [self loadPages];
        [self addSubview:self.hHPageViewerScrollView];
        
        
        self.selectedPageIndicatorView = [[UIView alloc] initWithFrame:CGRectMake(_hHPageViewerScrollViewFrame.origin.x, scrollViewY - 6, _titleButtonWidth, 4)];
        self.selectedPageIndicatorView.backgroundColor = [_hHPageViewerOptions objectForKey:hHPageViewerDisplayedPageIndicatorViewColorKey];
        [self addSubview:self.selectedPageIndicatorView];
        
    

    }
    return self;
}

-(void) initialiseDefaultOptions {
    
    hHPageTitleColorForDisplayedStateDefault = [UIColor darkGrayColor];
    hHPageTitleColorForNotDisplayedStateDefault = [UIColor lightGrayColor];
    
    hHDisplayedPageIndicatorViewColorDefault = [UIColor orangeColor];
    
    hHScrollToPageAnimatedDefault = @YES;
    
    _hHPageViewerKeyClassMap = @{hHPageViewerPageTitleColorForDisplayedStateKey: NSStringFromClass([UIColor class]),
                                 hHPageViewerPageTitleColorForNotDisplayedStateKey: NSStringFromClass([UIColor class]),
                                 hHPageViewerDisplayedPageIndicatorViewColorKey: NSStringFromClass([UIColor class]),
                                 hHPageViewerScrollToPageAnimatedKey: NSStringFromClass([NSNumber class])};
    

}

-(void) initialiseOptions {
    
    
    [self checkForGivenKeysAndValues];
    
    if (![_hHPageViewerOptions objectForKey:hHPageViewerPageTitleColorForDisplayedStateKey]) {
        [_hHPageViewerOptions setObject:hHPageTitleColorForDisplayedStateDefault forKey:hHPageViewerPageTitleColorForDisplayedStateKey];
    }
    if (![_hHPageViewerOptions objectForKey:hHPageViewerPageTitleColorForNotDisplayedStateKey]) {
        [_hHPageViewerOptions setObject:hHPageTitleColorForNotDisplayedStateDefault forKey:hHPageViewerPageTitleColorForNotDisplayedStateKey];
    }
    if (![_hHPageViewerOptions objectForKey:hHPageViewerDisplayedPageIndicatorViewColorKey]) {
        [_hHPageViewerOptions setObject:hHDisplayedPageIndicatorViewColorDefault forKey:hHPageViewerDisplayedPageIndicatorViewColorKey];
    }
    if (![_hHPageViewerOptions objectForKey:hHPageViewerScrollToPageAnimatedKey]) {
        [_hHPageViewerOptions setObject:hHScrollToPageAnimatedDefault forKey:hHPageViewerScrollToPageAnimatedKey];
    }




}
-(void) checkForGivenKeysAndValues {

    NSMutableDictionary *cleanOptions = [_hHPageViewerOptions mutableCopy];
    [_hHPageViewerOptions enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        if ([_hHPageViewerKeyClassMap.allKeys indexOfObjectIdenticalTo:key] == NSNotFound) {
            
            NSLog(@"[HHPageViewer] : ERROR given unrecognized key %@ in options with object %@", key, obj);
            [cleanOptions removeObjectForKey:key];
        } else if (![obj isKindOfClass:NSClassFromString(_hHPageViewerKeyClassMap[key])]) {
            NSLog(@"[HHPageViewer] : ERROR given %@ for key %@ was expecting Class %@ but got Class %@, passing default on instead",
                  obj,
                  key,
                  _hHPageViewerKeyClassMap[key],
                  NSStringFromClass([obj class]));
            [cleanOptions removeObjectForKey:key];
        }
    }];
    
    _hHPageViewerOptions = cleanOptions;

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (void)loadPages {
    
    _numberOfPages = [_datasource numberOfPagesInHHPageViewer:self];
    _titleButtonWidth = _hHPageViewerScrollViewFrame.size.width / _numberOfPages ;
    


    for (int i = 0; i < _numberOfPages; i++) {
        
        HHPageViewerPage *page = [[HHPageViewerPage alloc] init];
        page = [_datasource hHPageViewer:self viewForPageAtIndex:i];
        
        NSString *pageTitle = [_datasource hHPageViewer:self titleForPageAtIndex:i];
        [self addTitle:pageTitle AtIndex:i];
        
        CGRect frame = page.frame;
        frame.origin.y = 0;
        frame.origin.x = _hHPageViewerScrollViewFrame.size.width * i;
        
        page.frame = frame;
        
        [self.hHPageViewerScrollView addSubview:page];
    }
    
    self.hHPageViewerScrollView.contentSize = CGSizeMake(self.hHPageViewerScrollView.frame.size.width * _numberOfPages, self.hHPageViewerScrollView.frame.size.height);

    

}

- (void)addTitle:(NSString *)title AtIndex:(NSUInteger)index{

    
    float buttonHeight = _hHPageViewerScrollViewFrame.origin.y - 6 ;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame =CGRectMake(_titleButtonWidth*index, 0, _titleButtonWidth,buttonHeight);
    [btn setTitle:[_datasource hHPageViewer:self titleForPageAtIndex:index] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[btn titleLabel] setFont:[UIFont systemFontOfSize:14.0]];
    [btn setTitleColor:[_hHPageViewerOptions objectForKey:hHPageViewerPageTitleColorForNotDisplayedStateKey] forState:UIControlStateNormal];
    
    btn.tag = index;
    
    [btn addTarget:self action:@selector(scrollToRelativePage:) forControlEvents:UIControlEventTouchUpInside];
    
    if (index == 0) {
        
        [btn setTitleColor:[_hHPageViewerOptions objectForKey:hHPageViewerPageTitleColorForDisplayedStateKey] forState:UIControlStateNormal];
    }
    
    [_titlesBtnArray addObject:btn];
    [self addSubview:btn];

}
-(void) scrollToRelativePage:(UIButton *)btn {
    
    _pageTitlePressed = YES;
    
    CGRect frame;
    frame.origin.x = _hHPageViewerScrollViewFrame.size.width * btn.tag;
    frame.origin.y = 0;
    frame.size = _hHPageViewerScrollViewFrame.size;
    [self.hHPageViewerScrollView scrollRectToVisible:frame animated:[[_hHPageViewerOptions objectForKey:hHPageViewerScrollToPageAnimatedKey] boolValue]];
    
    [self scrollIndicatorViewToButton:btn];
    btn.enabled = NO;
}

-(void) scrollIndicatorViewToButton:(UIButton *)btn {
    
    
    int oldPageIndex = floor((self.selectedPageIndicatorView.frame.origin.x - _titleButtonWidth / 2) / _titleButtonWidth) + 1;
    
    if (oldPageIndex != btn.tag) {
        [[_titlesBtnArray objectAtIndex:oldPageIndex] setTitleColor:[_hHPageViewerOptions objectForKey:hHPageViewerPageTitleColorForNotDisplayedStateKey]
                                                               forState:UIControlStateNormal];
    }
    

    CGRect frame = CGRectMake(_titleButtonWidth * btn.tag, self.selectedPageIndicatorView.frame.origin.y, _titleButtonWidth, 4);
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.selectedPageIndicatorView.frame = frame;
        
    } completion:^(BOOL finished) {
        
        btn.enabled = YES;
        [btn setTitleColor:[_hHPageViewerOptions objectForKey:hHPageViewerPageTitleColorForDisplayedStateKey] forState:UIControlStateNormal];

    }];
    
}


#pragma mark-
#pragma mark UIScrollView Delegate

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = _hHPageViewerScrollViewFrame.size.width;
    int page = floor((self.hHPageViewerScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    CGFloat indicatorOffset  = self.hHPageViewerScrollView.contentOffset.x/_numberOfPages;
    [self moveIndicatorView:indicatorOffset];
    
    if (!_pageTitlePressed) {
        
        if (page > -1 && page < _numberOfPages) {
            [[_titlesBtnArray objectAtIndex:page] setTitleColor:[_hHPageViewerOptions objectForKey:hHPageViewerPageTitleColorForDisplayedStateKey] forState:UIControlStateNormal];

        } else  {
            NSLog(@"page out of bounds");
        }

        _pageTitlePressed = NO;
    }
    
    ScrollDirection scrollDirection;
    
    if (self.lastContentOffset > scrollView.contentOffset.x)
        scrollDirection = ScrollDirectionRight;
    else if (self.lastContentOffset < scrollView.contentOffset.x)
        scrollDirection = ScrollDirectionLeft;
    
    self.lastContentOffset = scrollView.contentOffset.x;
    
    // do whatever you need to with scrollDirection here.
    if (scrollDirection == ScrollDirectionLeft) {
        
        if (page > 0) {
            [[_titlesBtnArray objectAtIndex:page - 1] setTitleColor:[_hHPageViewerOptions objectForKey:hHPageViewerPageTitleColorForNotDisplayedStateKey] forState:UIControlStateNormal];

        }

    } else if (scrollDirection == ScrollDirectionRight) {
        
        if (page < _numberOfPages - 1) {
            [[_titlesBtnArray objectAtIndex:page + 1] setTitleColor:[_hHPageViewerOptions objectForKey:hHPageViewerPageTitleColorForNotDisplayedStateKey] forState:UIControlStateNormal];

        }

    }


}
-(void)moveIndicatorView:(float) contentOffset{
    
    CGRect frame = CGRectMake(contentOffset, self.selectedPageIndicatorView.frame.origin.y, self.selectedPageIndicatorView.frame.size.width, self.selectedPageIndicatorView.frame.size.height);
    self.selectedPageIndicatorView.frame = frame;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    _pageTitlePressed = NO;
}

@end
