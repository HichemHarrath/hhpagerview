//
//  ViewController.m
//  HHScrollView
//
//  Created by Hichem iMac on 14/05/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "ViewController.h"
#import "HHScrollViewViewController.h"

#import "HHPageViewer.h"
#import "HHPageViewerPage.h"

@interface ViewController () <HHPageViewerDatasource>

@property (nonatomic, retain) HHScrollViewViewController *hhscrollView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.titlesArray = [[NSMutableArray alloc] initWithArray:@[@"GREEN",@"BLUE",@"RED",@"BLACK", @"GRAY"]];
    
    HHPageViewer *hHpageViewer = [[HHPageViewer alloc] initWithFrame:CGRectMake(0, 64, 320, 512)
                                                          datasource:self
                                                          andOptions:@{hHPageViewerDisplayedPageIndicatorViewColorKey : [UIColor darkGrayColor],
                                                                                  hHPageViewerScrollToPageAnimatedKey : @YES ,
                                                                       hHPageViewerPageTitleColorForDisplayedStateKey : [UIColor darkGrayColor],
                                                                    hHPageViewerPageTitleColorForNotDisplayedStateKey : [UIColor lightGrayColor]}
                                  ];
    
    

    [self.view addSubview:hHpageViewer];

}

- (NSInteger)numberOfPagesInHHPageViewer:(HHPageViewer *)hHPageViewer{
    
//number of pages in HHpageViewer
    return [self.titlesArray count];
}

- (NSString *)hHPageViewer:(HHPageViewer *)hHPageViewer titleForPageAtIndex:(NSUInteger)index{
    
    return [self.titlesArray objectAtIndex:index];
}

-(HHPageViewerPage *)hHPageViewer:(HHPageViewer *)hHPageViewer viewForPageAtIndex:(NSUInteger)index {
    
    HHPageViewerPage *page = [[HHPageViewerPage  alloc] initWithFrame:CGRectMake(0, 64, 320, 512)];

    switch (index) {
        case 0:{
            page.backgroundColor = [UIColor greenColor];
            break;
        }
        case 1:{
            page.backgroundColor = [UIColor blueColor];
            break;
        }
        case 2:{
            page.backgroundColor = [UIColor redColor];
            break;
        }
        case 3:{
            page.backgroundColor = [UIColor blackColor];
            break;
        }
        case 4:{
            page.backgroundColor = [UIColor grayColor];
            break;
        }

        default:
            break;
    }
    
    return page;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
