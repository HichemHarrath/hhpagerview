//
//  HHScrollViewViewController.m
//  HHScrollView
//
//  Created by Hichem iMac on 14/05/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "HHScrollViewViewController.h"

@interface HHScrollViewViewController () <UIScrollViewDelegate>

@property (assign, nonatomic) CGRect hhScrollViewFrame;
@property (nonatomic, retain) NSArray *pagesArray;
@property (nonatomic, retain) NSArray *pagesTitles;

@property (nonatomic, assign) int pagesCount;
@property (nonatomic, assign) float buttonWidth;

@property(nonatomic, retain) UIView *selectedPageIndicatorView ;



@end

@implementation HHScrollViewViewController{
    
    BOOL _pageTitlePressed;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame Pages:(NSArray *)pagesArray Titles:(NSArray *)pagesTitles {
    self = [super init];
    
    if (self) {
        
        self.hhScrollViewFrame = frame;
        self.pagesArray = (pagesArray.count > 0) ? pagesArray: nil;
        self.pagesTitles = (pagesTitles.count > 0) ? pagesTitles: nil;
        
        self.pagesCount = [pagesArray count];
        
        self.buttonsHeight = 42;
        
        float indcatorViewY =self.hhScrollViewFrame.origin.y - 6 ;
        self.buttonWidth = self.hhScrollViewFrame.size.width / self.pagesCount ;

        
        self.selectedPageIndicatorView = [[UIView alloc] initWithFrame:CGRectMake(self.hhScrollViewFrame.origin.x, indcatorViewY, self.buttonWidth, 4)];
        self.selectedPageIndicatorView.backgroundColor = [UIColor grayColor];
        

    }

    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.hhScrollView  = [[UIScrollView alloc] initWithFrame:self.hhScrollViewFrame];
    self.hhScrollView.delegate = self;
    
    self.hhScrollView.showsHorizontalScrollIndicator = NO;
    self.hhScrollView.pagingEnabled = YES;
    
    [self loadPages];
    [self addPagesTitles];

    _pageTitlePressed = NO;
    
    [self.view addSubview:self.hhScrollView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadPages {
    
    for (int i = 0; i < self.pagesCount; i++) {
        
        UIView *pageView = [self.pagesArray objectAtIndex:i];
        
        CGRect frame = pageView.frame;
        frame.origin.y = 0;
        frame.origin.x = self.hhScrollViewFrame.size.width * i;
        
        pageView.frame = frame;
        
        [self.hhScrollView addSubview:pageView];
    }
    
    self.hhScrollView.contentSize = CGSizeMake(self.hhScrollView.frame.size.width * self.pagesCount, self.hhScrollView.frame.size.height);

}

-(void) addPagesTitles {
    
    [self.view addSubview:self.selectedPageIndicatorView] ;
    
    float buttonY = self.hhScrollViewFrame.origin.y - self.buttonsHeight - 8 ;
    
    
    for (int i = 0 ; i < self.pagesCount ; i++) {
    
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame =CGRectMake(self.buttonWidth*i, buttonY, self.buttonWidth, self.buttonsHeight);
        [btn setTitle:[self.pagesTitles objectAtIndex:i] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [[btn titleLabel] setFont:[UIFont systemFontOfSize:14.0]];
        
        
        btn.tag = i;
        [btn addTarget:self action:@selector(scrollToRelativePage:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:btn];
    }

}

-(void) scrollToRelativePage:(UIButton *)btn {
    
    _pageTitlePressed = YES;
    
    CGRect frame;
    frame.origin.x = self.hhScrollView.frame.size.width * btn.tag;
    frame.origin.y = 0;
    frame.size = self.hhScrollView.frame.size;
    [self.hhScrollView scrollRectToVisible:frame animated:YES];
    
    [self scrollIndicatorViewToButton:btn];
    btn.enabled = NO;
    
    
    

    
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.hhScrollView.frame.size.width;
    int page = floor((self.hhScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    

    if (!_pageTitlePressed) {
        [self scrollIndicatorViewToButtonIndex:page];
        _pageTitlePressed = NO;

        
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    _pageTitlePressed = NO;


}

-(void) scrollIndicatorViewToButtonIndex:(int ) index {
    
    
    CGRect frame = CGRectMake(self.buttonWidth * index, self.selectedPageIndicatorView.frame.origin.y, self.buttonWidth, 4);
    [UIView animateWithDuration:0.3 animations:^{
        
        self.selectedPageIndicatorView.frame = frame;
        
    }];

}
-(void) scrollIndicatorViewToButton:(UIButton *)btn {
    
    
    CGRect frame = CGRectMake(self.buttonWidth * btn.tag, self.selectedPageIndicatorView.frame.origin.y, self.buttonWidth, 4);
    [UIView animateWithDuration:0.3 animations:^{
        
        self.selectedPageIndicatorView.frame = frame;
    } completion:^(BOOL finished) {
        btn.enabled = YES;
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
