//
//  HHPageViewer.h
//  HHScrollView
//
//  Created by Hichem iMac on 15/05/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HHPageViewerPage.h"

typedef enum TitleTabType {
    TitleTypeNone,
    TitleTypeTextOnly,
    TitleTypeImageWithText,
} TitleTabType;


@protocol HHPageViewerDatasource ;



@interface HHPageViewer : UIView

@property (assign, nonatomic) NSUInteger numberOfPages;
@property (nonatomic, weak) id <HHPageViewerDatasource> datasource;
@property (nonatomic, strong) NSMutableDictionary *hHPageViewerOptions;

@property (nonatomic, retain) UIColor *selectedPageIndicatorViewColor;

@property (assign, nonatomic) TitleTabType titleTabType;



- (id)initWithFrame:(CGRect)frame datasource:(id <HHPageViewerDatasource>)datasource andOptions:(NSDictionary *)options;




extern NSString *const hHPageViewerPageTitleColorForDisplayedStateKey;
extern NSString *const hHPageViewerPageTitleColorForNotDisplayedStateKey;

extern NSString *const hHPageViewerDisplayedPageIndicatorViewColorKey;

extern NSString *const hHPageViewerScrollToPageAnimatedKey;



@end





@protocol HHPageViewerDatasource <NSObject>


@required
- (NSInteger)numberOfPagesInHHPageViewer:(HHPageViewer *)hHPageViewer;
- (NSString *)hHPageViewer:(HHPageViewer *)hHPageViewer titleForPageAtIndex:(NSUInteger)index;
- (HHPageViewerPage *)hHPageViewer:(HHPageViewer *)hHPageViewer viewForPageAtIndex:(NSUInteger)index;



@optional


@end