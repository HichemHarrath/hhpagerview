//
//  HHPageViewerPage.m
//  HHScrollView
//
//  Created by Hichem iMac on 15/05/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "HHPageViewerPage.h"

@implementation HHPageViewerPage

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
