//
//  HHPageViewerPageTitle.m
//  HHScrollView
//
//  Created by  HARRATH on 17/05/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "HHPageViewerPageTitle.h"




@implementation HHPageViewerPageTitle{
    TitleType _type ;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _type = TitleTypeNone;
        self = [self loadTitleView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame title:(NSString *)title{
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _type = TitleTypeTextOnly;
        self = [self loadTitleView];

    }
    return self;


}

- (id)initWithFrame:(CGRect)frame title:(NSString *)title titleImage:(UIImage *)titleImage{
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _type = TitleTypeImageWithText;
        self = [self loadTitleView];

    }
    return self;

}

- (id)loadTitleView{
    
    self.frame = self.titleViewFrame;

    
    switch (_type) {
        case TitleTypeNone:{
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:self.titleViewFrame];
            titleLabel.text = @"?";
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.contentMode = UIViewContentModeCenter;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [self addSubview:titleLabel];

            break;
        }
        case TitleTypeTextOnly:{
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:self.titleViewFrame];
            titleLabel.text = self.title;
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.contentMode = UIViewContentModeCenter;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [self addSubview:titleLabel];
            break;
        }
        case TitleTypeImageWithText:{
            
            CGRect imageFrame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height - (self.frame.size.height/3));
            CGRect labelFrame = CGRectMake(self.frame.origin.x, imageFrame.size.height, self.frame.size.width, self.frame.size.height -imageFrame.size.height);
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
            titleLabel.text = self.title;
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.contentMode = UIViewContentModeCenter;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [self addSubview:titleLabel];
            
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:imageFrame];
            imageView.contentMode = UIViewContentModeScaleAspectFit ;
            imageView.backgroundColor = [UIColor clearColor];
            [self addSubview:imageView];

            
            break;
        }
        default:
            break;
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)setTitleTextColor:(UIColor *)titleTextColor forState:(HHPageViewerPageTitleState)state {
    

}

@end
