//
//  HHScrollViewViewController.h
//  HHScrollView
//
//  Created by Hichem iMac on 14/05/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HHScrollViewViewController : UIViewController


@property (strong, nonatomic) UIScrollView * hhScrollView;

@property (nonatomic, assign) float buttonsHeight;


// titles colors normal and selected

@property (nonatomic, retain) UIColor *titleColorForNotDisplayedPage;
@property (nonatomic, retain) UIColor *titleColorForDisplayedPage;




-(id) initWithFrame:(CGRect )frame Pages:(NSArray *) pagesArray Titles:(NSArray *) pagesTitles;


@end
