//
//  HHPageViewerPageTitle.h
//  HHScrollView
//
//  Created by  HARRATH on 17/05/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum TitleType {
    TitleTypeNone,
    TitleTypeTextOnly,
    TitleTypeImageWithText,
} TitleType;


typedef enum HHPageViewerPageTitleState {
    HHPageViewerPageTitleStateSelected,
    HHPageViewerPageTitleStateNotSelected,
} HHPageViewerPageTitleState;

@interface HHPageViewerPageTitle : UIView

@property (assign, nonatomic) CGRect titleViewFrame;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic, strong) UIColor *titleColor;

@property (nonatomic, strong) UIImage *titleImageForSelectedState;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title;
- (id)initWithFrame:(CGRect)frame title:(NSString *)title titleImage:(UIImage *)titleImage;

- (void) setTitleTextColor:(UIColor *)titleTextColor forState:(HHPageViewerPageTitleState)state;



@end
